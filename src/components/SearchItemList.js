import React from "react";
import ModelItem from "./ModelItem";
import LessonPlanItem from "./LessonPlanItem";

class SearchItemList extends React.Component {
  handleLessonPlanList() {
    if (this.props.lessonPlans.length === 0) {
      return <h3>There are no Lesson Plans for this search!</h3>;
    } else {
      return this.props.lessonPlans.map((lessonPlan, index) => {
        return <LessonPlanItem key={index} lessonPlan={lessonPlan} />;
      });
    }
  }

  handleModelList() {
    if (this.props.models.length === 0) {
      return <h3>There are no Models for this search!</h3>;
    } else {
      return this.props.models.map((model, index) => {
        return <ModelItem key={index} model={model} />;
      });
    }
  }

  render() {
    return (
      <div>
        <h2>Lesson Plans list:</h2>
        {this.handleLessonPlanList()}
        <h2>Models list:</h2>
        {this.handleModelList()}
      </div>
    );
  }
}

export default SearchItemList;
