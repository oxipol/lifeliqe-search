import React from "react";

const ModelItem = ({ model }) => {
  return <li>{model.TextToDisplay}</li>;
};

export default ModelItem;
