import React from "react";

const LessonPlanItem = ({ lessonPlan }) => {
  return <li>{lessonPlan.TextToDisplay}</li>;
};

export default LessonPlanItem;
