import React from "react";
import { DebounceInput } from "react-debounce-input";

class SearchForm extends React.Component {
  static defaultProps = { languages: ["en-us", "es-es"] };
  state = {
    searchString: "",
    lang: "en-us"
  };

  handleSearchChange = event => {
    this.setState({ searchString: event.target.value }, () =>
      this.props.handleSearch(this.state.searchString, this.state.lang)
    );
  };

  handleLanguageChange = event => {
    this.setState({ lang: event.target.value }, () =>
      this.props.handleSearch(this.state.searchString, this.state.lang)
    );
  };

  render() {
    let languageOptions = this.props.languages.map(language => {
      return <option key={language}>{language}</option>;
    });

    return (
      <div>
        <DebounceInput
          placeholder="Find something!"
          debounceTimeout={150}
          onChange={this.handleSearchChange}
        />
        <select onChange={this.handleLanguageChange}>{languageOptions}</select>
      </div>
    );
  }
}

export default SearchForm;
