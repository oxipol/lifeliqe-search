import React, { Component } from "react";
import axios from "axios";

import SearchForm from "./components/SearchForm";
import SearchItemList from "./components/SearchItemList";

class App extends Component {
  state = {
    lessonPlans: [],
    models: []
  };

  handleSearch = (str, lang) => {
    if (str !== "") {
      axios
        .post(
          "https://online01.lifeliqe.com/search?searchStr=" +
            str +
            "&lang=" +
            lang +
            "&where=0"
        )
        .then(results => {
          this.setState({ lessonPlans: results.data.ResultsLessonPlans });
          this.setState({ models: results.data.ResultsModels });
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      this.setState({ lessonPlans: [] });
      this.setState({ models: [] });
    }
  };

  render() {
    const { lessonPlans, models } = this.state;
    return (
      <div>
        <SearchForm handleSearch={this.handleSearch} />
        <SearchItemList lessonPlans={lessonPlans} models={models} />
      </div>
    );
  }
}

export default App;
